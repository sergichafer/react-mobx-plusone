import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import commonStore from './stores/commonStore';
import authStore from './stores/authStore';

const superagent = superagentPromise(_superagent, global.Promise);

/* const API_ROOT = 'https://golang-echo-plusone.herokuapp.com/api'; */

const API_ROOT = 'http://localhost:1323/api';

const encode = encodeURIComponent;

const handleErrors = err => {
  if (err && err.response && err.response.status === 401) {
    authStore.logout();
  }
  return err;
};

const responseBody = res => res.body;

const tokenPlugin = req => {
  if (commonStore.token) {
    req.set('authorization', `Token ${commonStore.token}`);
  }
};

const requests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
};

const Auth = {
  current: () =>
    requests.get('/user'),
  login: (email, password) =>
    requests.post('/users/login', { user: { email, password } }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: user =>
    requests.put('/user', { user })
};

const Tags = {
  getAll: () => requests.get('/tags')
};

const Sports = {
  getAll: () => requests.get('/sports')
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = plan => Object.assign({}, plan, { slug: undefined })

const Plans = {
  all: (page, lim = 10) =>
    requests.get(`/plans?${limit(lim, page)}`),
  byAuthor: (author, page, query) =>
    requests.get(`/plans?author=${encode(author)}&${limit(5, page)}`),
  byTag: (tag, page, lim = 10) =>
    requests.get(`/plans?tag=${encode(tag)}&${limit(lim, page)}`),
  feed: () =>
    requests.get('/plans/feed?limit=3&offset=0'),
  del: slug =>
    requests.del(`/plans/${slug}`),
  get: slug =>
    requests.get(`/plans/${slug}`),
  update: plan =>
    requests.put(`/plans/${plan.slug}`, { plan: omitSlug(plan) }),
  joinPlan: plan =>
    requests.put(`/plans/${plan.slug}/join`, { plan: omitSlug(plan) }),
  create: plan =>
    requests.post('/plans', { plan })
};

const Contact = {
  sendMail: (email, subject, body) =>
    requests.post('/contact', { contactData: { email, subject, body } })
};

const Profile = {
  follow: username =>
    requests.post(`/profiles/${username}/follow`),
  get: username =>
    requests.get(`/profiles/${username}`),
  unfollow: username =>
    requests.del(`/profiles/${username}/follow`)
};

export default {
  Plans,
  Auth,
  Contact,
  Profile,
  Tags,
  Sports,
};
