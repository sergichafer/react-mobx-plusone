import { observable, action, reaction } from 'mobx';
import agent from '../agent';

class CommonStore {

  @observable appName = 'One2Join';
  @observable token = window.localStorage.getItem('jwt');
  @observable appLoaded = false;

  @observable tags = [];
  @observable sports = [];
  @observable isLoadingTags = false;
  @observable isLoadingSports = false;

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem('jwt', token);
        } else {
          window.localStorage.removeItem('jwt');
        }
      }
    );
  }

  @action loadTags() {
    this.isLoadingTags = true;
    return agent.Tags.getAll()
      .then(action(({ tags }) => { 
        tags ?
          this.tags = tags.map(t => t.toLowerCase()):
          this.tags = [""]; 
      }))
      .finally(action(() => { this.isLoadingTags = false; }))
  }
  
  @action loadSports() {
    this.isLoadingSports = true;
    return agent.Sports.getAll()
      .then(action(({ sports }) => { 
        sports ?
          this.sports = sports:
          this.sports = [{refer: 1, sport: "Football"}]; 
      }))
      .finally(action(() => { this.isLoadingSports = false; }))
  }

  @action setToken(token) {
    this.token = token;
  }

  @action setAppLoaded() {
    this.appLoaded = true;
  }

}

export default new CommonStore();
