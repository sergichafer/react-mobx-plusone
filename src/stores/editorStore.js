import { observable, action } from 'mobx';
import plansStore from './plansStore';
import moment from 'moment';
import toastr from 'toastr';
import { TOASTR_OPTIONS } from '../constants/Common';
toastr.options = TOASTR_OPTIONS;

class EditorStore {

  @observable inProgress = false;
  @observable errors = undefined;
  @observable planSlug = undefined;

  @observable title = '';
  @observable description = '';
  @observable image = '';
  @observable width = '';
  @observable height = '';
  @observable start_date = '';
  @observable end_date = '';
  @observable location = '';
  @observable sport = '';
  @observable tagList = [];

  @action setPlanSlug(planSlug) {
    if (this.planSlug !== planSlug) {
      this.reset();
      this.planSlug = planSlug;
    }
  }

  @action loadInitialData() {
    if (!this.planSlug) return Promise.resolve();
    this.inProgress = true;
    return plansStore.loadPlan(this.planSlug, { acceptCached: true })
      .then(action((plan) => {
        if (!plan) throw new Error('Can\'t load original plan');
        this.title = plan.title;
        this.description = plan.description;
        this.image = plan.image;
        this.width = plan.width;
        this.height = plan.height;
        this.start_date = new Date(moment(plan.start_date));
        this.end_date =  new Date(moment(plan.end_date));
        this.location = plan.location;
        this.sport = plan.sport.refer;
        this.tagList = plan.tagList;
      }))
      .finally(action(() => { this.inProgress = false; }));
  }

  @action reset() {
    this.title = '';
    this.description = '';
    this.image = '';
    this.width = '';
    this.height = '';
    this.start_date = '';
    this.end_date = '';
    this.location = '';
    this.sport = '';
    this.tagList = [];
  }

  @action setTitle(title) {
    this.title = title;
  }

  @action setDescription(description) {
    this.description = description;
  }

  @action setImage(image) {
    this.image = image;
  }

  @action setWidth(width) {
    this.width = width;
  }

  @action setHeight(height) {
    this.height = height;
  }

  @action setStartDate(start_date) {
    this.start_date = start_date;
  }

  @action setEndDate(end_date) {
    this.end_date = end_date;
  }

  @action setLocation(location) {
    this.location = location;
  }

  @action setSport(sport) {
    this.sport = sport;
  }

  @action addTag(tag) {
    if (this.tagList.includes(tag)) return;
    this.tagList.push(tag);
  }

  @action removeTag(tag) {
    this.tagList = this.tagList.filter(t => t !== tag);
  }

  @action submit() {
    this.inProgress = true;
    this.errors = undefined;
    const plan = {
      title: this.title,
      description: this.description,
      image: this.image,
      width: parseInt(this.width, 10),
      height: parseInt(this.height, 10),
      start_date: moment(this.start_date).format(),
      end_date: moment(this.end_date).format(),
      location: this.location,
      sport: parseInt(this.sport, 10),
      tagList: this.tagList,
      slug: this.planSlug,
    };
    return (this.planSlug ? plansStore.updatePlan(plan) : plansStore.createPlan(plan))
      .catch(action((err) => {
        toastr.error("Internal error, try again later!");
        this.errors = err.response && err.response.body && err.response.body.errors;
        throw err;
      }))
      .finally(action(() => { 
        toastr.success("Operation successfully completed!")
        this.inProgress = false; 
      }));
  }
}

export default new EditorStore();
