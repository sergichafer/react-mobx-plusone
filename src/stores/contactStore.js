import { observable, action, extendObservable } from 'mobx';
import agent from '../agent';
import toastr from 'toastr';
import { TOASTR_OPTIONS } from '../constants/Common';
toastr.options = TOASTR_OPTIONS;

class ContactStore {
  @observable inProgress = false;
  @observable errors = undefined;

  @observable values = {
    email: '',
    email_error:true,
    email_error_msg: '',    
    subject: '',
    subject_error:true,
    subject_error_msg: '', 
    body: '',
    body_error:true,
    body_error_msg: '', 
  };

  @action setEmail(email) {
    this.values.email_error = false;
    this.values.email = email;
  }

  @action setErrorEmail(email) {
    this.values.email = email;
    this.values.email_error = true;
    this.values.email_error_msg = 'Email must be valid';
  }

  @action setSubject(subject) {
    this.values.subject_error = false;
    this.values.subject = subject;
  }

  @action setErrorSubject(subject) {
    this.values.subject = subject;
    this.values.subject_error = true;
    this.values.subject_error_msg = 'Subject must be valid';
  }

  @action setBody(body) {
    this.values.body_error = false;
    this.values.body = body;
  }

  @action setErrorBody(body) {
    this.values.body = body;
    this.values.body_error = true;
    this.values.body_error_msg = 'Body must be valid';
  }

  @action reset() {
    extendObservable(this, {
      inProgress: false,
      errors: undefined,
      values:{
        email: '',
        email_error:true,
        email_error_msg: '',    
        subject: '',
        subject_error:true,
        subject_error_msg: '', 
        body: '',
        body_error:true,
        body_error_msg: ''
      }
    });
  }

  @action sendMail() {
    this.inProgress = true;
    this.errors = undefined;
    return agent.Contact.sendMail(this.values.email, this.values.subject, this.values.body)
      .then(() => toastr.success("Your mail has been sent!"))
      .catch(action((err) => {
        toastr.error("Internal error, try again later!");
        this.errors = err.response && err.response.body && err.response.body.errors;
        throw err;
      }))
      .finally(action(() => { 
        this.reset();
      }));
  }

}

export default new ContactStore();
