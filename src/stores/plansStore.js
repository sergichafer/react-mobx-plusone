import { observable, action, computed } from 'mobx';
import agent from '../agent';
import toastr from 'toastr';
import { TOASTR_OPTIONS } from '../constants/Common';
toastr.options = TOASTR_OPTIONS;

const LIMIT = 3;

export class PlansStore {

  @observable isLoading = false;
  @observable page = 0;
  @observable totalPagesCount = 0;
  @observable plansRegistry = observable.map();
  @observable predicate = {};

  @computed get plans() {
    return this.plansRegistry.values();
  };

  clear() {
    this.plansRegistry.clear();
    this.page = 0;
  }

  getPlan(slug) {
    return this.plansRegistry.get(slug);
  }

  @action setPage(page) {
    this.page = page;
  }

  @action setPredicate(predicate) {
    if (JSON.stringify(predicate) === JSON.stringify(this.predicate)) return;
    this.clear();
    this.predicate = predicate;
  }

  $req() {
    if (this.predicate.myFeed) return agent.Plans.feed(this.predicate.myFeed, this.page, LIMIT);
    if (this.predicate.tag) return agent.Plans.byTag(this.predicate.tag, this.page, LIMIT);
    if (this.predicate.author) return agent.Plans.byAuthor(this.predicate.author, this.page, LIMIT);
    return agent.Plans.all(this.page, LIMIT, this.predicate);
  }

  @action loadPlans() {
    this.isLoading = true;
    return this.$req()
      .then(action(({ plans, plansCount }) => {
        this.plansRegistry.clear();
        plans.forEach(plan => this.plansRegistry.set(plan.slug, plan));
        this.totalPagesCount = Math.ceil(plansCount / LIMIT);
      }))
      .finally(action(() => { this.isLoading = false; }));
  }

  @action loadPlan(slug, { acceptCached = false } = {}) {
    if (acceptCached) {
      const plan = this.getPlan(slug);
      if (plan) return Promise.resolve(plan);
    }
    this.isLoading = true;
    return agent.Plans.get(slug)
      .then(action(({ plan }) => {
        this.plansRegistry.set(plan.slug, plan);
        return plan;
      }))
      .finally(action(() => { this.isLoading = false; }));
  }

  @action createPlan(plan) {
    return agent.Plans.create(plan)
      .then(({ plan }) => {
        this.plansRegistry.set(plan.slug, plan);
        return plan;
      })
  }

  @action updatePlan(data) {
    return agent.Plans.update(data)
      .then(({ plan }) => {
        this.plansRegistry.set(plan.slug, plan);
        return plan;
      })
  }

  @action joinPlan(data) {
    return agent.Plans.joinPlan(data)
      .then(({ plan }) => {
        if (plan) {
          toastr.success("You have successfully joined the plan: "+plan.title+"!")
          return plan;
        }
      }).catch(action(err => { 
        toastr.error("Internal error, try again later!");
        throw err;
       }));
  }

  @action deletePlan(slug) {
    this.plansRegistry.delete(slug);
    return agent.Plans.del(slug)
      .catch(action(err => { this.loadPlans(); throw err; }));
  }
}

export default new PlansStore();
