import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
/**
 * @desc Prints the logged out view
 * @param {Props} props Contains parent component props
 * @return {DOMElement} Prints a dom element based on different conditions
 */
const LoggedOutView = props => {
  if (!props.currentUser) {
    return (
      <ul className="nav navbar-nav navbar-expand-lg ml-auto flex-nowrap">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/login" className="nav-link">
            Sign in
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/register" className="nav-link">
            Sign up
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/contact" className="nav-link">
            Contact
          </Link>
        </li>

      </ul>
    );
  }
  return null;
};
/**
 * @desc Prints the logged in view
 * @param {Props} props Contains parent component props
 * @return {DOMElement} Prints a dom element based on different conditions
 */
const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav navbar-nav ml-auto flex-nowrap">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/editor" className="nav-link">
            <i className="ion-compose" />&nbsp;New Plan
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/settings" className="nav-link">
            <i className="ion-gear-a" />&nbsp;Settings
          </Link>
        </li>

        <li className="nav-item">
          <Link
            to={`/@${props.currentUser.username}`}
            className="nav-link"
          > {
              props.currentUser.image ?
                <img src={props.currentUser.image} className="user-pic" alt="Profile pic" /> :
                <img src="https://cataas.com/cat" className="user-pic" alt="Profile pic" />
            }
            {props.currentUser.username}
          </Link>
        </li>

      </ul>
    );
  }

  return null;
};
/**
 * @desc Needed stores are injected into the component props
 */
@inject('userStore', 'commonStore')
@observer
/**
 * @desc Header component
 */
class Header extends React.Component {
  /**
   * @method render
   * @desc Render prints the navbar depending on the currentUser status
   * @return {DOMElement}
   */
  render() {
    return (
      <header>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="d-flex flex-grow-1">
            <span className="w-100 d-lg-none d-block"></span>
            <Link to="/" className="navbar-brand d-none d-lg-inline-block brand-name">{this.props.commonStore.appName}</Link>
          </div>
          <div className="collapse navbar-collapse flex-grow-1 text-right">

            <LoggedOutView currentUser={this.props.userStore.currentUser} />

            <LoggedInView currentUser={this.props.userStore.currentUser} />
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
