import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('userStore', 'commonStore')
@observer
/**
 * @desc Private route component
 */
export default class PrivateRoute extends React.Component {
  /**
   * @method render
   * @desc Renders a component that allows redirection to components only when user is logged in
   * @return {DOMElement}
   */
  render() {
    const { userStore, ...restProps } = this.props;
    if (userStore.currentUser) return <Route {...restProps} />;
    return <Redirect to="/" />;
  }
}
