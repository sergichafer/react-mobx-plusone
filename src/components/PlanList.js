import PlanPreview from './PlanPreview';
import ListPagination from './ListPagination';
import LoadingSpinner from './LoadingSpinner';
import React from 'react';
/**
 * @desc Creates an structure of cards for each event
 * @param {Props} props Contains the props of the parent component
 */
const PlanList = props => {
  if (props.loading && props.plans.length === 0) {
    return (
      <LoadingSpinner />
    );
  }

  if (props.plans.length === 0) {
    return (
      <div className="plan-preview">
        No plans are here... yet.
      </div>
    );
  }

  return (
    <div>
      <div className="container d-flex">
        <div className="d-flex justify-content-center mb-auto p-2">
          <div className="card-deck">
            {
              props.plans.map(plan => {
                return (
                  <PlanPreview plan={plan} key={plan.slug} />
                );
              })
            }
          </div>
        </div>
      </div>
      <div  className="container d-flex justify-content-center m-5">
        <ListPagination
          onSetPage={props.onSetPage}
          totalPagesCount={props.totalPagesCount}
          currentPage={props.currentPage}
        />
      </div>
    </div>
  );
};

export default PlanList;
