import { withRouter } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import { inject, observer } from 'mobx-react';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('contactStore')
@withRouter
@observer
/**
 * @desc Contact component
 */
export default class Contact extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered to reset the form
   */
  componentDidMount() {
    this.props.contactStore.reset();
  }
  /**
   * @function handleEmailChange
   * @param {Event} e Input event
   * @desc Handles email input change and validates the field
   */
  handleEmailChange = e => {
    (e.target.value.length < 10)?
    this.props.contactStore.setErrorEmail(e.target.value):
    this.props.contactStore.setEmail(e.target.value)
  };
  /**
   * @function handleSubjectChange
   * @param {Event} e Input event
   * @desc Handles subject input change and validates the field
   */
  handleSubjectChange = e => {
    (e.target.value.length < 10)?
    this.props.contactStore.setErrorSubject(e.target.value):
    this.props.contactStore.setSubject(e.target.value)
  };
  /**
   * @function handleBodyChange
   * @param {Event} e Input event
   * @desc Handles body input change and validates the field
   */
  handleBodyChange = e => {
    (e.target.value.length < 10)?
    this.props.contactStore.setErrorBody(e.target.value):
    this.props.contactStore.setBody(e.target.value)
  };
  /**
   * @function handleEmailChange
   * @param {Event} e Input event
   * @desc Handles email send function and redirects after it
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.contactStore.sendMail()
      .then(() => this.props.history.replace('/'));
  };
  /**
   * @method render
   * @desc Render prints the contact form
   * @return {DOMElement}
   */
  render() {
    const { values, errors, inProgress } = this.props.contactStore;

    return (
      <div className="contact-page">
        <div className="container page">
          <div className="row">

            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Contact Us</h1>

              <ListErrors errors={errors} />

              <form onSubmit={this.handleSubmitForm}>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="email"
                      placeholder="Email"
                      value={values.email}
                      onChange={this.handleEmailChange}
                      autoFocus />
                    <small hidden={!values.email_error}>{values.email_error_msg}</small>
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="name"
                      placeholder="Subject"
                      value={values.subject}
                      onChange={this.handleSubjectChange} />
                    <small hidden={!values.subject_error}>{values.subject_error_msg}</small>
                  </fieldset>

                  <fieldset className="form-group">
                    <textarea
                      className="form-control form-control-lg"
                      placeholder="Body"
                      value={values.body}
                      onChange={this.handleBodyChange}>
                    </textarea>
                    <small hidden={!values.body_error}>{values.body_error_msg}</small>
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={inProgress || values.email_error || values.subject_error || values.body_error}>
                    Send
                  </button>

                </fieldset>
              </form>
            </div>

          </div>
        </div>
      </div>
    );
  }
}
