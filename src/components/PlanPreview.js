import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('plansStore')
@observer
/**
 * @desc Plan preview component
 */
export default class PlanPreview extends React.Component {
  /**
   * @method render
   * @desc Render prints the plan data in an adapted design based on bootstaps cards
   * @return {DOMElement}
   */
  render() {
    const { plan } = this.props;

    return (
      <div className={`card col-md-12 ${ plan.active === 2 ? 'disabled' : null }`}>
        <br />
        <div className="card-body">
          <Link to={`/@${plan.author.username}`} className="card-title">
            {
              plan.author.image ?
                <img src={plan.author.image} className="user-pic" alt="Profile pic" /> :
                <img src="https://cataas.com/cat" className="user-pic" alt="Profile pic" />
            }
          </Link>
          <Link to={`/plan/${plan.slug}`}>
            <img className="card-img-top" src={plan.image} alt="" />
          </Link>
        </div>
        <div className="card-body">
          <h5 className="card-text">{plan.title}</h5>
          <p className="card-text">{plan.description.length > 60 ? plan.description.substring(0,60)+"..." : plan.description}</p>
        </div>
        <div className="card-body">
            <Link to={`/plan/${plan.slug}`} className="btn btn-primary card-link" styles="color:white;">Read More...</Link>
        </div>
        <div className="card-footer bg-white">
          <small className="text-muted">{new Date(plan.start_date).toDateString()}</small>
        </div>
      </div>
    );
  }
}
