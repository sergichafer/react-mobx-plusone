import ListErrors from './ListErrors';
import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
/* import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete'; */
/**
 * @desc Needed stores are injected into the component props
 */
@inject('editorStore', 'commonStore')
@withRouter
@observer
/**
 * @desc Editor component used in both plan creation and plan update
 */
export default class Editor extends React.Component {

  state = {
    tagInput: '',
  };
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered to load all necessary data for the component
   */
  componentDidMount() {
    this.props.commonStore.loadSports();
    this.props.editorStore.setPlanSlug(this.props.match.params.slug);
    this.props.commonStore.loadSports();
    this.props.editorStore.loadInitialData();
  }
  /**
   * @method componentDidMount
   * @desc Invoked after the component props have been updated
   */
  componentDidUpdate(prevProps) {
    if (this.props.match.params.slug !== prevProps.match.params.slug) {
      this.props.commonStore.loadSports();
      this.props.editorStore.setPlanSlug(this.props.match.params.slug);
      this.props.editorStore.loadInitialData();
    }
  }
  /**
   * @function changeTitle
   * @param {Event} e Input event
   * @desc Handles title input change
   */
  changeTitle = e => this.props.editorStore.setTitle(e.target.value);
  /**
   * @function changeDescription
   * @param {Event} e Input event
   * @desc Handles description input change
   */
  changeDescription = e => this.props.editorStore.setDescription(e.target.value);
  /**
   * @function changeImage
   * @param {Event} e Input event
   * @desc Handles image input change
   */
  changeImage = e => this.props.editorStore.setImage(e.target.value);
  /**
   * @function changeWidth
   * @param {Event} e Input event
   * @desc Handles width input change
   */
  changeWidth = e => this.props.editorStore.setWidth(e.target.value);
  /**
   * @function changeHeight
   * @param {Event} e Input event
   * @desc Handles height input change
   */
  changeHeight = e => this.props.editorStore.setHeight(e.target.value);
  /**
   * @function changeStartDate
   * @param {Event} e Input event
   * @desc Handles start date input change
   */
  changeStartDate = e => this.props.editorStore.setStartDate(e);
  /**
   * @function changeEndDate
   * @param {Event} e Input event
   * @desc Handles end date input change
   */
  changeEndDate = e => this.props.editorStore.setEndDate(e);
  /**
   * @function changeLocation
   * @param {Event} e Input event
   * @desc Handles location input change
   */
  changeLocation = e => this.props.editorStore.setLocation(e.target.value);
  /* {
    return this.props.editorStore.setLocation(e)
  }; */
  /**
   * @function changeSport
   * @param {Event} e Input event
   * @desc Handles sport input change
   */
  changeSport = e => this.props.editorStore.setSport(e.target.value);
  /**
   * @function changeTagInput
   * @param {Event} e Input event
   * @desc Handles tag input change
   */
  changeTagInput = e => this.setState({ tagInput: e.target.value });
  /**
   * @function handleTagInputKeyDown
   * @param {Event} ev Input event
   * @desc Handles input keys cases for the tag field
   */
  handleTagInputKeyDown = ev => {
    switch (ev.keyCode) {
      case 13: // Enter
      case 9: // Tab
      case 188: // ,
        if (ev.keyCode !== 9) ev.preventDefault();
        this.handleAddTag();
        break;
      default:
        break;
    }
  };
  /**
   * @function handleAddTag
   * @desc Handles tag add to the component state
   */
  handleAddTag = () => {
    if (this.state.tagInput) {
      this.props.editorStore.addTag(this.state.tagInput.trim());
      this.setState({ tagInput: '' });
    }
  };
  /**
   * @function handleRemoveTag
   * @desc Handles tag removal from the component state
   */
  handleRemoveTag = tag => {
    if (this.props.editorStore.inProgress) return;
    this.props.editorStore.removeTag(tag);
  };

  /* handleSelect = location => {
    geocodeByAddress(location)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log('Success', latLng))
      .catch(error => console.error('Error', error));
  }; */
  /**
   * @function submitForm
   * @param {Event} ev Input event
   * @desc Handles plan creation/modification submit input and resets the component fields
   */
  submitForm = ev => {
    ev.preventDefault();
    const { editorStore } = this.props;
    editorStore.submit()
      .then(plan => {
        editorStore.reset();
        this.props.history.replace(`/plan/${plan.slug}`)
      });
  };
  /**
   * @method render
   * @desc Render prints the editor form
   * @return {DOMElement}
   */
  render() {
    const {
      inProgress,
      errors,
      title,
      description,
      image,
      width,
      height,
      start_date,
      end_date,
      location,
      sport,
      tagList,
    } = this.props.editorStore;
    const { sports } = this.props.commonStore;
    return (
      <div className="editor-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-10 offset-md-1 col-xs-12">

              <ListErrors errors={errors} />

              <form>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="text"
                      placeholder="Plan Title"
                      value={title}
                      onChange={this.changeTitle}
                      disabled={inProgress}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="What's this plan about?"
                      value={description}
                      onChange={this.changeDescription}
                      disabled={inProgress}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="url"
                      placeholder="URL of the image"
                      value={image}
                      onChange={this.changeImage}
                      disabled={inProgress}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="number"
                      placeholder="Width of the field?"
                      value={width}
                      onChange={this.changeWidth}
                      disabled={inProgress}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="number"
                      placeholder="Height of the field?"
                      value={height}
                      onChange={this.changeHeight}
                      disabled={inProgress}
                    />
                  </fieldset>
                  <fieldset className="form-group">
                    <DatePicker
                        className="form-control mr-4"
                        minDate={Date.now()}
                        selected={start_date}
                        onChange={this.changeStartDate}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        dateFormat="dd-MM-YYYY hh:mm:ss"
                        timeCaption="time"
                        disabled={inProgress}
                    />
                    <DatePicker
                        className="form-control ml-4"
                        minDate={Date.now()}
                        selected={end_date}
                        onChange={this.changeEndDate}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        dateFormat="dd-MM-YYYY hh:mm:ss"
                        timeCaption="time"
                        disabled={inProgress}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Location of the field?"
                      value={location}
                      onChange={this.changeLocation}
                      disabled={inProgress}
                    />
                    {/* <PlacesAutocomplete
                      value={location}
                      onChange={this.changeLocation}
                      onSelect={this.handleSelect}
                    >
                      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <div>
                          <input
                            {...getInputProps({
                              placeholder: 'Search Places ...',
                              className: 'location-search-input',
                            })}
                          />
                          <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                              const className = suggestion.active
                                ? 'suggestion-item--active'
                                : 'suggestion-item';
                              // inline style for demonstration purpose
                              const style = suggestion.active
                                ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                : { backgroundColor: '#ffffff', cursor: 'pointer' };
                              return (
                                <div
                                  {...getSuggestionItemProps(suggestion, {
                                    className,
                                    style,
                                  })}
                                >
                                  <span>{suggestion.description}</span>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      )}
                    </PlacesAutocomplete> */}
                  </fieldset>

                  <fieldset className="form-group">
                    <select
                      className="form-control"
                      value={sport}
                      onChange={this.changeSport}>
                      {
                        sports.map(sport => {
                          return (
                            <option key={sport.refer} id={sport.refer} value={sport.refer}>{sport.sport}</option>
                          );
                        })
                      }
                    </select>
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Enter tags"
                      value={this.state.tagInput}
                      onChange={this.changeTagInput}
                      onBlur={this.handleAddTag}
                      onKeyDown={this.handleTagInputKeyDown}
                      disabled={inProgress}
                    />

                    <div className="tag-list">
                      {
                        tagList.map(tag => {
                          return (
                            <span className="tag-default tag-pill" key={tag}>
                              <i
                                className="ion-close-round"
                                onClick={() => this.handleRemoveTag(tag)}
                              />
                              {tag}
                            </span>
                          );
                        })
                      }
                    </div>
                  </fieldset>

                  <button
                    className="btn btn-lg pull-xs-right btn-primary float-right"
                    type="button"
                    disabled={inProgress}
                    onClick={this.submitForm}
                  >
                    Publish Plan
                  </button>

                </fieldset>
              </form>

            </div>
          </div>
        </div>
      </div>
    );
  }
}
