import { Link } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import { inject, observer } from 'mobx-react';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('authStore')
@observer
/**
 * @desc Register component
 */
export default class Register extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered to reset the form
   */
  componentDidMount() {
    this.props.authStore.reset();
  }
  /**
   * @function handleUsernameChange
   * @param {Event} e Input event
   * @desc Handles username input change
   */
  handleUsernameChange = e => this.props.authStore.setUsername(e.target.value);
  /**
   * @function handleEmailChange
   * @param {Event} e Input event
   * @desc Handles email input change
   */
  handleEmailChange = e => this.props.authStore.setEmail(e.target.value);
  /**
   * @function handlePasswordChange
   * @param {Event} e Input event
   * @desc Handles password input change
   */
  handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);
  /**
   * @function handleSubmitForm
   * @param {Event} e Input event
   * @desc Handles submit input click
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.register()
      .then(() => this.props.history.replace('/'));
  };
  /**
   * @method render
   * @desc Renders the register form component
   * @return {DOMElement}
   */
  render() {
    const { values, errors, inProgress } = this.props.authStore;

    return (
      <div className="auth-page">
        <div className="container page">
          <div className="row">

            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Sign Up</h1>
              <p className="text-xs-center">
                <Link to="login">
                  Have an account?
                </Link>
              </p>

              <ListErrors errors={errors} />

              <form onSubmit={this.handleSubmitForm}>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="text"
                      placeholder="Username"
                      value={values.username}
                      onChange={this.handleUsernameChange}
                      autoFocus
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="email"
                      placeholder="Email"
                      value={values.email}
                      onChange={this.handleEmailChange}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="password"
                      placeholder="Password"
                      value={values.password}
                      onChange={this.handlePasswordChange}
                    />
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={inProgress}
                  >
                    Sign in
                  </button>

                </fieldset>
              </form>
            </div>

          </div>
        </div>
      </div>
    );
  }
}
