import PlanList from './PlanList';
import React from 'react';
import LoadingSpinner from './LoadingSpinner';
import RedError from './RedError';
import { Link, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
/**
 * @desc Link to profile settings is provided within this class
 * @param {Props} props Contains the props of the parent component
 */
const EditProfileSettings = props => {
  if (props.isUser) {
    return (
      <Link
        to="/settings"
        className="btn btn-sm btn-outline-secondary action-btn">
        <i className="ion-gear-a" /> Edit Profile Settings
      </Link>
    );
  }
  return null;
};
/**
 * @desc Needed stores are injected into the component props
 */
@inject('plansStore', 'profileStore', 'userStore')
@withRouter
@observer
/**
 * @desc Profile component
 */
export default class Profile extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered to reset the form
   */
  componentDidMount() {
    this.props.plansStore.setPredicate(this.getPredicate());
    this.props.profileStore.loadProfile(this.props.match.params.username);
    this.props.plansStore.loadPlans();
  }
  /**
   * @method componentDidMount
   * @desc Invoked after the component props have been updated
   */
  componentDidUpdate(previousProps) {
    if (this.props.location !== previousProps.location) {
      this.props.profileStore.loadProfile(this.props.match.params.username);
      this.props.plansStore.setPredicate(this.getPredicate());
      this.props.plansStore.loadPlans();
    }
  }
  /**
   * @function getTab
   * @desc This function is used to determine the selected view
   */
  getTab() {
    return 'all'
  }
  /**
   * @function getPredicate
   * @desc This function is used to determine which filter will be applied to the query
   */
  getPredicate() {
    switch (this.getTab()) {
      default: return { author: this.props.match.params.username }
    }
  }
  /**
   * @function handleSetPage
   * @param {String} page Contains the next page to load
   * @desc This function is used to determine which page will be set and to refresh store props
   */
  handleSetPage = page => {
    this.props.plansStore.setPage(page);
    this.props.plansStore.loadPlans();
  };
  /**
   * @method render
   * @desc Renders a component that shows the user plans and a link to his profile update form
   * @return {DOMElement}
   */
  render() {
    const { profileStore, plansStore, userStore } = this.props;
    const { profile, isLoadingProfile } = profileStore;
    const { currentUser } = userStore;

    if (isLoadingProfile && !profile) return <LoadingSpinner />;
    if (!profile) return <RedError message="Can't load profile" />;

    const isUser = currentUser && profile.username === currentUser.username;

    return (
      <div className="profile-page">

        <div className="user-info">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-10 offset-md-1">
                {
                  profile.image ?
                    <img src={profile.image} className="user-pic--plan" alt="Profile pic" /> :
                    <img src="https://cataas.com/cat" className="user-pic--plan" alt="Profile pic" />
                }
                <h4>{profile.username}</h4>
                <p>{profile.bio}</p>

                <EditProfileSettings isUser={isUser} />

              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-10 offset-md-1">
              <PlanList
                plans={plansStore.plans}
                totalPagesCount={plansStore.totalPagesCount}
                onSetPage={this.handleSetPage}
                loading={plansStore.isLoading}
              />
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export { Profile };
