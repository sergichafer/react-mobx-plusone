import Header from './Header';
import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import PrivateRoute from './PrivateRoute';

import Plan from './Plan';
import Editor from './Editor';
import Home from './Home';
import Login from './Login';
import Contact from './Contact';
import Profile from './Profile';
import Register from './Register';
import Settings from './Settings';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('userStore', 'commonStore')
@withRouter
@observer
/**
 * @desc App base component with the app router
 */
export default class App extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered
   */
  componentDidMount() {
    if (this.props.commonStore.token) {
      this.props.userStore.pullUser()
        .finally(() => this.props.commonStore.setAppLoaded());
    } else {
      this.props.commonStore.setAppLoaded();
    }
  }
  /**
   * @method render
   * @desc Render prints the navbar and defines the routes depending on the state of the current user
   * @return {DOMElement}
   */
  render() {
    if (this.props.commonStore.appLoaded) {
      return (
        <div>
          <Header />
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/contact" component={Contact} />
            <Route path="/register" component={Register} />
            <PrivateRoute path="/editor/:slug?" component={Editor} />
            <Route path="/plan/:id" component={Plan} />
            <PrivateRoute path="/settings" component={Settings} />
            <Route path="/@:username" component={Profile} />
            <Route path="/@:username/favorites" component={Profile} />
            <Route path="/" component={Home} />
          </Switch>
        </div>
      );
    }
    return (
      <Header />
    );
  }
}
