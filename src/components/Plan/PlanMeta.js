import PlanActions from './PlanActions';
import { Link } from 'react-router-dom';
import React from 'react';
import { observer } from 'mobx-react';
/**
 * @desc Prints the plan meta and is based on a observer in case any of the props are modified
 * @param {Props} props contains the parent properties
 * @return {DOMElement} Prints a dom element with the plan meta and loads the action in case the user created the plan
 */
const PlanMeta = observer(props => {
  const plan = props.plan;
  return (
    <div className="plan-meta">
      <Link to={`/@${plan.author.username}`}>
        {
          plan.author.image ?
            <img src={plan.author.image} className="user-pic--plan" alt="Profile pic" /> :
            <img src="https://cataas.com/cat" className="user-pic--plan" alt="Profile pic" />
        }
      </Link>
      <div className="d-flex justify-content-center row">
        <div className="badge badge-light col-md-2 m-1 p-2">
          <Link to={`/@${plan.author.username}`} className="author">
            {plan.author.username}
          </Link>
        </div>
        
      </div>

      <PlanActions canModify={props.canModify} plan={plan} onDelete={props.onDelete} />
    </div>
  );
});

export default PlanMeta;
