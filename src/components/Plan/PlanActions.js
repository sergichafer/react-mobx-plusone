import { Link } from 'react-router-dom';
import React from 'react';
/**
 * @desc Prints the actions that the owner can do with the plan
 * @param {Props} props contains the parent properties
 * @return {DOMElement} Prints a dom element with the actions
 */
const PlanActions = props => {
  const plan = props.plan;
  const handleDelete = () => props.onDelete(plan.slug);

  if (props.canModify) {
    return (
      <span>

        <Link
          to={`/editor/${plan.slug}`}
          className="btn btn-secondary btn-sm m-2"
        >
          <i className="ion-edit" /> Edit Plan
        </Link>

        <button className="btn btn-danger btn-sm m-2" onClick={handleDelete}>
          <i className="ion-trash-a" /> Delete Plan
        </button>

      </span>
    );
  }

  return (
    <span />
  );
};

export default PlanActions;
