/**
 * @desc exported sports to be loaded dynamically inside the plan component
  */
export {default as Football} from './football';
export {default as Soccer} from './soccer';
export {default as Tennis} from './tennis';