import React from 'react';
import './sport.css';
/**
 * @desc Dynamic react component that is being exported and prints an svg in case it is imported
 * @return {DOMElement} Prints an svg of a footbal ball
 */
class NFL extends React.Component {
    render() {
      return (
        <div className="icon-wrapper">
          <svg id="netball-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
            <title>Football</title>
            <g id="rugby">
                <g id="ball">
                    <path className="cls-1" d="M31.51,35.49C47.57,19.43,67.84,15.15,74.38,21.7s2.26,26.81-13.8,42.87S24.41,85.06,17.71,78.37s-2.26-26.81,13.8-42.87Z" />
                    <path className="cls-1" d="M17.71,78.37c.77-9.07,12.87-24.26,22.56-34s20.64-17.71,33.9-22.9" />
                    <path className="cls-1" d="M36.09,43.6l2.52,2.52,2.5,2.5M32.5,47.41,35,50l2.48,2.48M29.31,51a21.51,21.51,0,0,0,2.38,2.82A21.84,21.84,0,0,0,34.33,56m-7.51-.68a9.11,9.11,0,0,0,1.75,2.36,11.47,11.47,0,0,0,2.09,1.5m9.12-19.06,2.41,2.41,2.61,2.61m-1.44-8.21a19.74,19.74,0,0,1,2.58,2.15A20.53,20.53,0,0,1,48.38,42m-.68-7.51a10,10,0,0,1,2,1.43,10.6,10.6,0,0,1,1.82,2.41" />
                    <path className="cls-2" d="M17,63.6a34.4,34.4,0,0,0,3.67,6.12c3.18,4.19,7,6.92,11.41,9.47M18.78,56.55a38.66,38.66,0,0,0,5,7.79C28.32,69.9,34.53,75,40.69,76.91M58.84,21.73a34.16,34.16,0,0,1,6.4,3.89,36.29,36.29,0,0,1,9.19,11.19M51.79,23.53a38.89,38.89,0,0,1,8,5.21c5.48,4.49,10.44,10.63,12.36,16.71" />
                </g>
                <path id="air" className="cls-1" d="M87.89,85.36,68.74,66.52M78.55,84.7,65.11,71.48m.6,9.41L59.76,75" />
            </g>
          </svg>

        </div>
      );
    }
  }
  
  export default NFL;