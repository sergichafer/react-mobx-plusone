import React from 'react';
import './sport.css';
/**
 * @desc Dynamic react component that is being exported and prints an svg in case it is imported
 * @return {DOMElement} Prints an svg of a tennis racket
 */
class Tennis extends React.Component {
    render() {
      return (
        <div className="icon-wrapper">
            <svg id="tennis-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                <title>Tennis</title>
                <g id="racket">
                    <g id="handle">
                        <path className="cls-1" d="M57.16,56.44c-10,3.26-16.6,9.65-23.91,15.88" />
                        <path className="cls-1" d="M29.72,69.72l0,0s10.92-12,12-25.52C40.74,59,29.72,69.72,29.72,69.72Z" />
                        <polygon className="cls-1" points="30.68 70.72 33.03 72.98 17.31 90.09 13 85.89 29.68 69.72 30.68 70.72" />
                        <path className="cls-2" d="M27.47,71.86,28.64,77l.08.29m-6-1.23,1.61,5.67,0,.09m-4.43,4.55-1.63-5.77" />
                    </g>
                    <g id="pad">
                        <path className="cls-3" d="M48.34,22.58C56.88,12.4,70.14,9.46,78,16s7.22,20.12-1.32,30.31S54.83,59.44,47,52.88s-7.22-20.12,1.32-30.31Z" />
                        <line className="cls-4" x1="61.21" y1="13.62" x2="83.21" y2="32.08" />
                        <line className="cls-4" x1="68.23" y1="12.93" x2="82.68" y2="25.05" />
                        <line className="cls-4" x1="56.12" y1="15.94" x2="81.82" y2="37.5" />
                        <line className="cls-4" x1="51.89" y1="18.97" x2="79.56" y2="42.19" />
                        <line className="cls-4" x1="48.34" y1="22.58" x2="76.63" y2="46.32" />
                        <line className="cls-4" x1="45.4" y1="26.7" x2="73.07" y2="49.92" />
                        <line className="cls-4" x1="43.15" y1="31.4" x2="68.84" y2="52.96" />
                        <line className="cls-4" x1="41.75" y1="36.81" x2="63.75" y2="55.27" />
                        <line className="cls-4" x1="42.02" y1="35.28" x2="59.75" y2="14.15" />
                        <line className="cls-4" x1="42.2" y1="42.92" x2="67.71" y2="12.52" />
                        <path className="cls-4" d="M43.74,48.94l.59-.7L46.23,46l3.25-3.87,3.24-3.86L56,34.37l3.24-3.86,3.24-3.87,3.24-3.86,3.25-3.87,3.24-3.86,1.32-1.58h0M47.6,52.18l2.5-3,3.24-3.87,3.24-3.86,3.25-3.87,3.24-3.86,3.24-3.87L69.56,26l3.24-3.87L76,18.29,78,16M51.47,55.43l.59-.7L54,52.45l3.24-3.86,3.24-3.86,3.25-3.87L66.94,37l3.25-3.86,3.24-3.87,3.24-3.86,3.24-3.87L81.24,20h0" />
                        <line className="cls-4" x1="57.25" y1="56.38" x2="83.17" y2="25.49" />
                        <line className="cls-4" x1="65.21" y1="54.74" x2="82.94" y2="33.61" />
                    </g>
                </g>
                <g id="ball">
                    <path className="cls-5" d="M80.25,71.17c-2.14,6-8-.59-8.89,3.12" />
                    <path className="cls-6" d="M79.58,70.65a5,5,0,1,1-7.05.62,5,5,0,0,1,7.05-.62Z" />
                </g>
            </svg>

        </div>
      );
    }
  }
  
  export default Tennis;