import React from 'react'
import * as components from './components'
/**
 * @desc Dynamic array of objects that after being filtered returns a react component
 * @return Returns a react component
 */
let sports = [
    {
        "key": "football",
        "component": <components.Football />
    },
    {
        "key": "soccer",
        "component": <components.Soccer />
    },
    {
        "key": "tennis",
        "component": <components.Tennis />
    }
];

export default sports;