import PlanMeta from './PlanMeta';
import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import RedError from '../RedError';
import marked from 'marked';
import sports from "./plan_sport";
import moment from 'moment';
/**
 * @desc Needed stores are injected into the component props
 */
@inject('plansStore', 'userStore')
@withRouter
@observer
/**
 * @desc Single plan view component
 */
export default class Plan extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered
   */
  componentDidMount() {
    const slug = this.props.match.params.id;
    this.props.plansStore.loadPlan(slug, { acceptCached: true });
  }
  /**
   * @function handleDeletePlan
   * @param {String} slug
   * @desc Handles the plan deletion based on the plan slug
   */
  handleDeletePlan = slug => {
    this.props.plansStore.deletePlan(slug)
      .then(() => this.props.history.replace('/'));
  };
  /**
   * @function handleDeletePlan
   * @param {Event} event prevents default actions
   * @param {Object} plan plan data from the store
   * @param {String} username username of the user to join the plan
   * @desc Handles the plan join event
   */
  handleJoinPlan = (event, plan, username) => {
    event.preventDefault();
    if (!plan.participant && plan.participant !== username) {
      plan.participant = username;
      this.props.plansStore.joinPlan(plan)
        .then(() => this.props.history.replace('/'));
    }
  };
  /**
   * @function getSportsComponent
   * @param {String} term sport to load
   * @desc Dynamically loads a react component with a svg of the sport
   */
  getSportsComponent(term){
    return sports.filter(x => x.key.toLowerCase().includes(term.toLowerCase()))
  }
  /**
   * @method render
   * @desc Render prints the single plan view dinamically based on different conditions
   * @return {DOMElement}
   */
  render() {
    const slug = this.props.match.params.id;
    const { currentUser } = this.props.userStore;
    const plan = this.props.plansStore.getPlan(slug);
    /**
     * @desc If plan is not present error component is printed 
     */
    if (!plan) return <RedError message="Can't load plan" />;
    const markup = { __html: marked(plan.description, { sanitize: true }) };
    /**
     * @desc Constant used to check if the user can modify the plan
     */
    const canModify = currentUser && currentUser.username === plan.author.username;
    /**
     * @desc Constant used to check if the user can join the plan
     */
    const canJoin = currentUser && plan.active === 0 ? 
      plan.participants.filter(participant => participant.profile.username === currentUser.username):
      false;
    /**
     * @desc Constants used to check when the plan will start or end
     */
    const currentDate = moment();
    const future = moment(plan.start_date);
    const futureEnd = moment(plan.end_date);
    return (
      <div className="plan-page">
        <h2 className="display-6">
          {plan.title}
        </h2>
        {
          currentUser && !canModify && canJoin.length === 0 ?

          <button type="button" className="btn btn-outline-info float-right" onClick={
            (e) => {
              this.handleJoinPlan(e, plan, currentUser.username)
            }
          }>Join Plan</button> :

          null
        }
        <div className="container page">
      
          <div className="row plan-content">
            <div className="col-md-12">
              {
                (this.getSportsComponent(plan.sport.sport)[0])?

                this.getSportsComponent(plan.sport.sport)[0].component:
                
                null
              }
              <h4>Description</h4>
              <p dangerouslySetInnerHTML={markup} />
              <h4>Utilities</h4>
              <ul className="tag-list">
                {
                  plan.tagList.map(tag => {
                    return (
                      <li
                        className="tag-default tag-pill tag-outline"
                        key={tag}
                      >
                        {tag}
                      </li>
                    );
                  })
                }
              </ul>

            </div>
          </div>
          <hr />   
          <div className="col-md-12 alert alert-primary" role="alert">
            {
            currentDate <= future ?
              ((moment.duration(future.diff(currentDate) > 120) ?
              `This plan will start in the following date ${moment(future).format('DD/MM/YYYY')}`:
              `This plan will start in ${Math.round(moment.duration(future.diff(currentDate)).asMinutes())} minutes`)):
              currentDate < futureEnd ?
                `This plan already started and will end in ${Math.round(moment.duration(futureEnd.diff(currentDate)).asMinutes())} minutes!`:
                `This plan has ended!`
            }
          </div>
          <hr />
          <div className="plan-actions" />
        </div>
        <div className="jumbotron jumbotron-fluid">
          <div className="container">
            <center>
              <PlanMeta
                plan={plan}
                canModify={canModify}
                onDelete={this.handleDeletePlan}
              />
            </center>
          </div>
        </div>
      </div>
    );
  }
}
