import React from 'react';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../LoadingSpinner';
/**
 * @desc Prints the list of tabs and in case of a click in one of them
 * adds them to the search query params to filter plans based on the tag
 * @param {Props} props contains the parent properties
 * @return {DOMElement} Prints a list of tags or a loading spinner
 * component in case tags are still being processed
 */
const Tags = props => {
  const tags = props.tags;
  if (tags) {
    return (
      <div className="tag-list">
        {
          tags.map(tag => {

            return (
              <Link
                to={{
                  pathname: "/",
                  search: "?tab=tag&tag=" + tag
                }}
                className="tag-default ion-pound"
                key={tag}
              >&nbsp;{tag}
              </Link>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <LoadingSpinner />
    );
  }
};

export default Tags;
