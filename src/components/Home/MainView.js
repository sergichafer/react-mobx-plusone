import PlanList from '../PlanList';
import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, NavLink } from 'react-router-dom'
import { parse as qsParse } from 'query-string';
/**
 * @desc Adds the tab with the user feed, with the plans he joined
 * @param {Props} props contains the parent properties
 */
const YourFeedTab = props => {
  if (props.currentUser) {

    return (
      <li className="nav-item">
      <NavLink
          className="nav-link"
          isActive={
            (match, location) => {
              return location.search.match("tab=feed") ? 1 : 0;
            }
          }
          to={{
            pathname: "/",
            search: "?tab=feed"
          }}
        >
          Your Feed
          <span className="ml-2 badge badge-dark">{props.soonEvent}</span>
        </NavLink>
      </li>
    );
  }
  return null;
};
/**
 * @desc Adds the tab representation of the global state of the plans
 * @param {Props} props contains the parent properties
 */
const GlobalFeedTab = props => {
  return (
    <li className="nav-item">
      <NavLink
        className="nav-link"
        isActive={
          (match, location) => {
            return !location.search.match(/tab=(feed|tag)/) ? 1 : 0;
          }
        }
        to={{
          pathname: "/",
          search: "?tab=all"
        }}
      >
        Global Feed
      </NavLink>
    </li>
  );
};
/**
 * @desc Add the the tab based on the selected tag
 * @param {Props} props contains the parent properties
 */
const TagFilterTab = props => {
  if (!props.tag) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound" /> {props.tag}
      </a>
    </li>
  );
};
/**
 * @desc Needed stores are injected into the component props
 */
@inject('plansStore', 'commonStore', 'userStore')
@withRouter
@observer
/**
 * @desc MainView of the app
 */
export default class MainView extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered
   */
  componentDidMount() {
    this.props.plansStore.setPredicate(this.getPredicate());
    this.props.plansStore.loadPlans();
  }
  /**
   * @method componentDidUpdate
   * @param {Props} props contains the previous and the new state of the properties
   * @desc Invoked after the component props have been updated
   */
  componentDidUpdate(previousProps) {
    if (
      this.getTab(this.props) !== this.getTab(previousProps) ||
      this.getTag(this.props) !== this.getTag(previousProps)
    ) {
      this.props.plansStore.setPredicate(this.getPredicate());
      this.props.plansStore.loadPlans();
    }
  }
  /**
   * @function getTag
   * @param {Props} props contains the props loaded from the store
   * @desc This function is used to get the new tag from the url param
   */
  getTag(props = this.props) {
    return qsParse(props.location.search).tag || "";
  }
  /**
   * @function getTab
   * @param {Props} props contains the props loaded from the store
   * @desc This function is used to get the new tab from the url param
   */
  getTab(props = this.props) {
    return qsParse(props.location.search).tab || 'all';
  }
  /**
   * @function getPredicate
   * @param {Props} props contains the props loaded from the store
   * @desc This function is used to check which tab is selected
   */
  getPredicate(props = this.props) {
    switch (this.getTab(props)) {
      case 'feed': return { myFeed: true };
      case 'tag': return { tag: qsParse(props.location.search).tag };
      default: return {};
    }
  }
  /**
   * @function handleTabChange
   * @param {String} tab Contains the next tab to select
   * @desc This function is used to determine which tab will be selected and which endpoint will be fetched
   */
  handleTabChange = (tab) => {
    if (this.props.location.query.tab === tab) return;
    this.props.router.push({ ...this.props.location, query: { tab } })
  };
  /**
   * @function handleTabChange
   * @param {String} page Contains the next page to be printed
   * @desc Sets the new page and refreshes the plans
   */
  handleSetPage = page => {
    this.props.plansStore.setPage(page);
    this.props.plansStore.loadPlans();
  };
  /**
   * @method render
   * @desc Render prints the main view dinamically based on different conditions
   * @return {DOMElement}
   */
  render() {
    const { currentUser } = this.props.userStore;
    const { plans, isLoading, page, totalPagesCount } = this.props.plansStore;
    let soonEvent = 0;
    if (currentUser){
      plans.forEach(function(plan) {
        if (plan.participants.filter(participant => participant.profile.username === currentUser.username) && plan.active === 1) {
          soonEvent = soonEvent + 1;
        }
      });
    }
    return (
      <div className="col-md-9">
        <div className="feed-toggle">
          <ul className="nav nav-tabs">

            <YourFeedTab
              currentUser={currentUser}
              tab={this.getTab()}
              soonEvent={soonEvent}
            />

            <GlobalFeedTab
              tab={this.getTab()}
            />

            <TagFilterTab tag={qsParse(this.props.location.search).tag} />

          </ul>
        </div>

        <PlanList
          plans={plans}
          loading={isLoading}
          totalPagesCount={totalPagesCount}
          currentPage={page}
          onSetPage={this.handleSetPage}
        />
      </div>
    );
  }
};
