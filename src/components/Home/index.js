import Banner from './Banner';
import MainView from './MainView';
import React from 'react';
import Tags from './Tags';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

/**
 * @desc Needed stores are injected into the component props
 */
@inject('commonStore')
@withRouter
@observer
/**
 * @desc Home component
 */
export default class Home extends React.Component {
  /**
   * @method componentDidMount
   * @desc Invoked after the component has been rendered
   */
  componentDidMount() {
    this.props.commonStore.loadTags();
  }
  /**
   * @method render
   * @desc Render prints the main view dinamically based on different conditions
   * @return {DOMElement}
   */
  render() {
    const { tags, token, appName } = this.props.commonStore;
    return (
      <div className="home-page">

        <Banner token={token} appName={appName} />

        <div className="container page">
          <div className="row">
            <MainView />

            <div className="col-md-3">
              <div className="sidebar">

                <p>Popular Tags</p>

                <Tags
                  tags={tags}
                />

              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
