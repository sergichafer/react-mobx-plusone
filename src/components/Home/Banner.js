import React from 'react';
/**
 * @desc Prints the banner of the app when the client is not logged in
 * @param {String} appName Name of the app to be printed
 * @param {String} token User token to be checked
 * @return {DOMElement} Prints a dom element based on different conditions
 */
const Banner = ({ appName, token }) => {
  if (token) {
    return null;
  }
  return (
    <div className="jumbotron jumbotron-fluid">
      <div className="container">
        <h1 className="display-4">
          {appName}
        </h1>
        <p className="lead">Never get bored!</p>
      </div>
    </div>
  );
};

export default Banner;
