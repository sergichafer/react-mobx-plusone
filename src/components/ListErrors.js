import React from 'react';
/**
 * @desc ListErrors component
 */
class ListErrors extends React.Component {
  /**
   * @method render
   * @desc Render prints the errors iterating over the props provided
   * @return {DOMElement}
   */
  render() {
    const errors = this.props.errors;
    if (errors) {
      return (
        <ul className="error-messages">
          {
            Object.keys(errors).map(key => {
              return (
                <li key={key}>
                  <b>{key.charAt(0).toUpperCase()+key.slice(1)} field:</b> {errors[key]}
                </li>
              );
            })
          }
        </ul>
      );
    } else {
      return null;
    }
  }
}

export default ListErrors;
