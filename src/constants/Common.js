/**
 * @desc variables for the toastr notifications of the app
 */
export const TOASTR_OPTIONS = {
    closeButton: true,
    preventDuplicated: true,
    positionClass: 'toast-top-right',
    progressBar: true
}