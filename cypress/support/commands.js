// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
const app = 'http://localhost:3000/#/';

Cypress.Commands.add('login', () => { 
    cy.visit(app+'login');
    cy.get('input[type=email]').type('sergislm4@gmail.com', {force: true})
    cy.get('input[type=password]').last().type('123456', {force: true});
    cy.get('button[type=submit]').last().click();
})
Cypress.Commands.add('loginCyTest', () => { 
    cy.visit(app+'login');
    cy.get('input[type=email]').type('test@test.com', {force: true})
    cy.get('input[type=password]').last().type('123456', {force: true});
    cy.get('button[type=submit]').last().click();
})

