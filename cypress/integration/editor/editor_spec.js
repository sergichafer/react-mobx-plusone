const app = 'http://localhost:3000/#/';
import moment from 'moment';

describe('Editor tests', function() {
    before(function() {
      cy.login();
    });
    it('Create a plan', function() {
      cy.wait(1000);
      cy.visit(app+'editor');
      const plan = [
        {
          'key': 'title',
          'test_case': 'Test at my house',
        },
        {
          'key': 'about',
          'test_case': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        {
          'key': 'start_date',
          'test_case': moment().format("dd-MMM-yyyy hh:mm:ss").toString(),
        },
        {
          'key': 'end_date',
          'test_case': moment().add(120, 'minutes').format("dd-MMM-yyyy hh:mm:ss").toString(),
        },
        {
          'key': 'location',
          'test_case': 'Valencia',
        },
        {
          'key': 'tag',
          'test_case': 'ball',
        },
      ];
      const size = [
        {
          'key': 'width',
          'test_case': '444',
        },
        {
          'key': 'height',
          'test_case': '444',
        },
      ];
      cy.get('input[type=text]').each((element, i) => {
        cy.wrap(element).type(plan[i].test_case, {force: true});
      });
      cy.get('input[type=number]').each((element, i) => {
        cy.wrap(element).type(size[i].test_case, {force: true});
      });
      cy.get('input[type=url]').type('http://is3.mzstatic.com/image/thumb/Purple5/v4/74/29/9e/74299ee6-b408-0a2a-935f-fc0b35e03c2f/source/512x512bb.jpg', {force: true});
      cy.get('button[type=button]').last().click();
      cy.url().should('eq', app+'plan/test-at-my-house');
    });
    it('Edit the plan', function() {
      cy.login();
      cy.wait(1000);
      cy.visit(app+'plan/test-at-my-house');
      cy.get('.plan-meta a').last().click();
      cy.url().should('eq', app+'editor/test-at-my-house');
      cy.get('input[type=text]').first().type(' 2');
      cy.get('button[type=button]').last().click();
      cy.url().should('eq', app+'plan/test-at-my-house-2');
      cy.get('h2').first().contains('Test at my house 2');
  });
});