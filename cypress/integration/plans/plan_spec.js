const app = 'http://localhost:3000/#/';

describe('Plan offline page tests', function() {
    beforeEach(function() {
        cy.visit(app);
    });
    it('Check if the plan from the editor is being displayed', function() {
        cy.get('.card').length > 0;
    });
});

describe('Plan online page tests', function() {
    it('User can join a plan and ', function() {
        cy.loginCyTest();
        cy.wait(1000);
        cy.visit(app+'plan/test-at-my-house-2');
        cy.get('button').first().click();
        cy.url().should('eq', app); 
    });
    it('Plan contains user meta', function() {
        cy.login();
        cy.wait(1000);
        cy.visit(app+'plan/test-at-my-house-2');
        cy.get('p').first().contains('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
        cy.get('svg title').contains('Football');
        cy.get('.plan-meta a').length > 1;
    });
    it('Delete the plan', function() {
        cy.login();
        cy.wait(1000);
        cy.visit(app+'plan/test-at-my-house-2');
        cy.get('.plan-meta button').last().click();
        cy.url().should('eq', app);
    });
});