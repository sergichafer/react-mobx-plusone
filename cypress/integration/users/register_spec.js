const app = 'http://localhost:3000/#/';

describe('Register page tests', function() {
    beforeEach(function() {
        cy.visit(app+'register');
    });
    it('Verify name input is focused on load.', function() {
        cy.focused().lenght > 0;
    });

    it('Try a unsuccessful register with invalid fields.', function() {
        cy.get('input[type=text]').last().type('test', {force: true});
        cy.get('input[type=email]').last().type('dsddsd@s', {force: true});
        cy.get('input[type=password]').last().type('asdfgh', {force: true});
        cy.get('button[type=submit]').last().click();
        cy.url().should('eq', app+'register');
    });

    it('Try a successful register.', function() {
        cy.get('input[type=text]').last().type('cypress', {force: true});
        cy.get('input[type=email]').last().type('cy@cy.com', {force: true});
        cy.get('input[type=password]').last().type('123456', {force: true});
        cy.get('button[type=submit]').last().click();
        cy.url().should('eq', app);
    });
});