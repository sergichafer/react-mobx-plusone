const app = 'http://localhost:3000/#/';

describe('Login page tests', function() {
  beforeEach(function() {
    cy.visit(app+'login');
  });
  it('Verify email input is focused on load.', function() {
    cy.focused().lenght > 0;
  });

  it('Try a unsuccessful login with invalid fields.', function() {
    cy.get('input[type=email]').last().type('test@test.com', {force: true});
    cy.get('input[type=password]').last().type('asdfgh', {force: true});
    cy.get('button[type=submit]').last().click();
    cy.url().should('eq', app+ 'login');
  });

  it('Try a successful login.', function() {
    cy.login();
    cy.url().should('eq', app);
    cy.reload();
    cy.url().should('eq', app);
  });

  it('Test logout.', function() {
    cy.login();
    cy.url().should('eq', app);
    cy.visit(app+'settings');
    cy.get('button').last().click();
    cy.url().should('eq', app);
  });
});

