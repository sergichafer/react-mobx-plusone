const app = 'http://localhost:3000/#/';

describe('Contact page tests', function() {
  beforeEach(function() {
    cy.visit(app+'contact');
  });
  it('Verify email input is focused on load.', function() {
    cy.focused().lenght > 0;
  });
  it('Blank fields.', function() {
    cy.get('input[type=email]').click();
    cy.get('input[type=name]').click();
    cy.get('textarea').click();
    cy.get('button').last().should('have', 'attr.disabled');
  });
  it('Try a successful contact.', function() {
    const contactCase = [
        {
            'key': 'email',
            'test_case': 'sergislm4@gmail.com',
        },
        {
            'key': 'subject',
            'test_case': 'Hola test, Im testing',
        },
        {
            'key': 'body',
            'test_case': 'Lorem Ipsum',
        },
    ];
    cy.get('input[type=email]').type(contactCase[0].test_case, {force: true});
    cy.get('input[type=name]').type(contactCase[1].test_case, {force: true});
    cy.get('textarea').type(contactCase[2].test_case, {force: true});
    cy.get('button').last().click();
    cy.url().should('eq', app);
  });

});

