const app = 'http://localhost:3000/#/';

describe('Profile update test', function() {
    it('User can update profile ', function() {
        cy.login();
        cy.wait(1000);
        cy.visit(app+'settings');
        cy.get('input[type=text]').last().type('4');
        cy.get('button').first().click();
        cy.wait(1000);
        cy.visit(app+'@sergislm44');
        cy.get('h4').first().contains('sergislm44');
        cy.visit(app+'settings');
        cy.get('input[type=text]').last().clear();
        cy.get('input[type=text]').last().type('sergislm4');
        cy.get('button').first().click();
        cy.wait(1000);
        cy.visit(app+'@sergislm4');
        cy.get('h4').first().contains('sergislm4');
    });
});