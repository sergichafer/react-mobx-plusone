# PlusOne

A react/mobx frontend educational purposes project

## Getting Started

This is an that has been built based on a few requirements:

HOMEPAGE;
LOGIN;
REGISTER;
PROFILE;
PROFILE SETTINGS;
LIST;
DINAMIC SUBCOMPONENT LOADING;
DETAILS;
CONTACT;

> ### React + Mobx codebase containing real world examples (CRUD, auth, advanced patterns, etc) using [Golang/Echo]
(https://github.com/sergislm4/golang-echo-plusone) API.


## Getting started

To get the frontend running locally:

- Clone this repo
- `npm install` to install all required dependencies
- `npm start` to start the local server (this project uses create-react-app)

## Functionality overview

**General functionality:**

- Authenticate users via JWT (login/signup pages + logout button on settings page)
- CRU* users (sign up & settings page - no deleting required)
- CRUD Plans
- GET and display paginated lists of Plans
- GET and display paginated lists of subcomponents dinamically depending on the API

**The general page breakdown looks like this:**

- Home page (URL: /#/ )
    - List of tags
    - List of plans pulled from either Global or by Tag
    - Pagination for list of plans
- Sign in/Sign up pages (URL: /#/login, /#/register )
    - Use JWT (store the token in localStorage)
- Settings page (URL: /#/settings )
- Editor page to create/edit plans (URL: /#/editor, /#/editor/plan-slug-here )
- Plan page (URL: /#/plan/plan-slug-here )
    - Delete plan button (only shown to plan's author)
    - Render markdown from server client side
    - Dinamic sport component with svg animation
- Profile page (URL: /#/@username )
    - Show basic user info

<br />

## Authors

* **Sergi Chàfer** 

## License 

This project was based on the useful goThinkster frontends/backends repos.
* [GoThinkster Realworld App Repo](https://github.com/gothinkster/realworld) - Realworld App

All of the codebases are MIT licensed unless otherwise specified.